﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Gives us acces to unitys built in UI tools
using UnityEngine.UI;
public class ShowsUI : MonoBehaviour
{
    public Canvas overlayScreen;



    void OnTriggerEnter(Collider TheThingThatWalkedIntoMe)
    {
        if(TheThingThatWalkedIntoMe.tag == "Player")
        {
            //Shows the UI canvas
            overlayScreen.enabled = true;
            Debug.Log("You Entered the Area");
        }
    }
   
    void OnTriggerExit(Collider TheThingThatWalkedIntoMe)
    {
        if (TheThingThatWalkedIntoMe.tag == "Player")
        {
            //Hides the UI canvas
            overlayScreen.enabled = false;
            Debug.Log("You Left the Area");
        }
    }
}
